/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */
package com.mycompany.xoproject;

/**
 *
 * @author user
 */
import java.util.Scanner;

public class XOproject {

    public int row;
    public int col;
    Scanner kb = new Scanner(System.in);
    public boolean play = false;
    public String start;
    public boolean end;
    public String turn;
    public String[][] board = {{"_", "_", "_"}, {"_", "_", "_"}, {"_", "_", "_"}};

    public void startGame() {
        System.out.println("Welcome to  OX game!!!");
        System.out.print("Start Game??? (y/n): ");
        start = kb.nextLine().toLowerCase();
        while (!start.equals("n") && !start.equals("y")) {
            System.out.print("Start Game (y/n): ");
            start = kb.nextLine().toLowerCase();
        }
        if (start.equals("n")) {
            play = false;
        } else {
            play = true;
        }

    }

  public void inputRowAndCol() {
        System.out.print("Input row : ");
        row = kb.nextInt();
        System.out.print("Input column : ");
        col = kb.nextInt();
        if (((row > 0 && row < 4) && (col > 0 && col < 4))) {

            if (board[row - 1][col - 1].equals("_")) {

                board[row - 1][col - 1] = turn.toUpperCase();
                playerWin();
                nextTurn();

            } else {

                while ((!(row > 0 && row < 4) && !(col > 0 && col < 4)) && !(board[row - 1][col - 1].equals("_"))) {

                    System.out.println("Please Input Again.");
                    System.out.print("Input row :");
                    row = kb.nextInt();
                    System.out.print("Input column :");
                    col = kb.nextInt();

                }

                return;
            }

        } else {
            return;
        }
    }

    public void nextTurn() {
        if (turn.equals("x")) {
            turn = "o";
        } else {
            turn = "x";
        }

    }

    public boolean playerDraw() {
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board.length; j++) {
                if (board[i][j].equals("_")) {
                    return false;
                }
            }
        }
        return true;
    }

    public void playerWin() {
        if (checkRows() || checkColumns() || checkDiagonals()) {

            System.out.println("+--------------------+");
            System.out.println("|    PLAYER " + turn + " WINS!   |");
            System.out.println("+--------------------+");
            if (endGame()) {
                reset();
            } else {
                play = false;

            }
        } if(play == true) {
            if (playerDraw()) {
                System.out.println("+--------------------+");
                System.out.println("|    PLAYER DRAW!    |");
                System.out.println("+--------------------+");
                if (endGame()) {
                    reset();
                } else {
                    play = false;

                }
            }
        }
    }

    public boolean checkRows() {
        for (int j = 0; j < board[row - 1].length; j++) {
            if (!board[row - 1][j].toLowerCase().equals(turn)) {
                return false;
            }
        }
        return true;
    }

    public boolean checkDiagonals() {
        if (board[0][0].toLowerCase().equals(turn) && board[1][1].toLowerCase().equals(turn) && board[2][2].toLowerCase().equals(turn)) {
            return true;
        }

        if (board[0][2].toLowerCase().equals(turn) && board[1][1].toLowerCase().equals(turn) && board[2][0].toLowerCase().equals(turn)) {
            return true;
        }

        return false;
    }

    public boolean checkColumns() {
        for (int j = 0; j < board[0].length; j++) {
            if (board[0][j].toLowerCase().equals(turn) && board[1][j].toLowerCase().equals(turn) && board[2][j].toLowerCase().equals(turn)) {
                return true;
            }
        }
        return false;
    }

    public void showBoard() {
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board[i].length; j++) {
                System.out.print(board[i][j] + " ");

            }
            System.out.println();

        }

    }

    public boolean endGame() {
        String con = kb.nextLine().toLowerCase();

        while (!con.equals("n") && !con.equals("y")) {

            System.out.print("Do you want to Exit ?(y/n): ");
            con = kb.nextLine().toLowerCase();
        }
        if (con.equals("y")) {
            return false;
        }
        return true;

    }

    public void reset() {
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board.length; j++) {
                board[i][j] = "_";
            }
        }
        turn = "o";
    }

    public void showTurn() {
        System.out.println("_________________________");
        System.out.println("Turn -> " + turn.toUpperCase());

    }

    public void showProcess() {
        showBoard();
        showTurn();
        inputRowAndCol();

    }

    public static void main(String[] args) {

         XOproject project = new  XOproject();
        Scanner kb = new Scanner(System.in);
        project.startGame();
        if (project.play == false) {
            System.out.println("Good Bye!");
            return;
        }

        project.turn = "x";
        while (project.play) {
            project.showProcess();
        }
    }

}
